#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <client_ros/clientclass.h>
#include <ros/ros.h>
#include <QQmlEngine>
#include <QQmlContext>
#include <QtQml>
#include <qnamespace.h>
int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QCoreApplication::setAttribute(Qt::AA_NativeWindows);
    ros::init(argc, argv, "client");
    qmlRegisterType<clientClass>("clientClass",1,0,"ClientClass");
    QQmlApplicationEngine engine;
//    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    engine.load(QUrl::fromLocalFile("main.qml"));
return app.exec();
}
