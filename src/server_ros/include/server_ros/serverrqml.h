#ifndef SERVERRQML_H
#define SERVERRQML_H

#include <QObject>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <iostream>
#include <QObject>
#include <QUdpSocket>
#include <QtNetwork>
#include <ros/ros.h>
#include "std_msgs/String.h"

class serverrqml : public QObject
{
    Q_OBJECT
public:
    explicit serverrqml(QObject *parent = nullptr);
    void getDatagram(const std_msgs::String::ConstPtr &msg1);
    void sendDatagram();
    int average(int num, int howmany);
private:
    QString m_val,msgStr;
    QJsonDocument m_doc;
    QJsonDocument m_doc1;
    QJsonObject m_set;
    QJsonValue m_value;
    int m_fromClientInt = 0;
    int m_count = 0;
    int m_ave = 0;
    int m_sum1 = 0;
    int m_count1 = 0;
    int m_averagenum = 0;

    QJsonObject m_averageNum;
    QByteArray m_datagram2;
    int sum = 0;
    ros::NodeHandle nh;
    ros::Subscriber server_sub;
    ros::Publisher server_pub;
    std_msgs::String msg;
   // ros::Rate loop_rate;
    bool trueorfalse = true;
   // std_msgs::String msg;
    std::string std_msgss;
    std::stringstream ss;
    QVariant msgs_topic,send_topic;
    QString string_topic;
signals:


public slots:
//    void getDatagram(const std_msgs::String::ConstPtr &msg);
//    void sendDatagram();
//    int average(int num, int howmany);
};


#endif // SERVERRQML_H
