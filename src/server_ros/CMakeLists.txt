cmake_minimum_required(VERSION 2.8.3)
project(server_ros)


find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  qt_build
)

file(GLOB_RECURSE SOURCES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} FOLLOW_SYMLINKS src/*.cpp include/server_ros/*.hpp include/server_ros/*.h)
include_directories(include ${catkin_INCLUDE_DIRS})
#################################
find_package(Qt5Core REQUIRED)
find_package(Qt5Sql REQUIRED)
find_package(Qt5Widgets REQUIRED)
#find_package(Qt5Charts REQUIRED)
find_package(Qt5Quick REQUIRED)
find_package(Qt5Multimedia REQUIRED)
#find_package(Qt5QuickControls2)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

#set configs
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)

QT5_ADD_RESOURCES(QT_RESOURCES_CPP ${QT_RESOURCES})
QT5_WRAP_UI(QT_FORMS_HPP ${QT_FORMS})
QT5_WRAP_CPP(QT_MOC_HPP ${QT_MOC})
include_directories(
    ${Qt5Core_INCLUDE_DIRS}
    ${Qt5Gui_INCLUDE_DIRS}
    ${Qt5Quick_INCLUDE_DIRS}
   # ${Qt5Quick.Controls2_INCLUDE_DIRS}
    ${Qt5Widgets_INCLUDE_DIRS}
    ${Qt5PrintSupport_INCLUDE_DIRS}
    ${Qt5Qml_INCLUDE_DIRS}
    ./src
    ${Qt5Sql_INCLUDE_DIRS}
   # ${Qt5Charts_INCLUDE_DIRS}
    ${Qt5Multimedia_INCLUDE_DIRS}
    ${QT_INCLUDE_DIR}
    )

add_definitions( -std=c++11 -fPIC)

catkin_package(
 INCLUDE_DIRS
  include
  CATKIN_DEPENDS
  std_msgs
  roscpp

)
include_directories(include)
include_directories(${catkin_INCLUDE_DIRS})
include_directories(${Eigen_INCLUDE_DIRS})

add_executable(server_ros_node src/server_ros_node.cpp
src/serverrqml.cpp
include/server_ros/serverrqml.h
# ${coreheaders}
 #   ${corecpps}
)
target_link_libraries(server_ros_node ${catkin_LIBRARIES})
target_link_libraries(server_ros_node
        Qt5::Core
        Qt5::Widgets
        Qt5::Quick
        Qt5::Sql
        Qt5::Multimedia
     #   Qt5::Charts

        )
add_dependencies(server_ros_node server_ros_generate_messages_cpp_qt_build)
#add_dependencies(server_ros_node ${${PROJECT_NAME}_EXPORTED_TARGETS})

#add_library(serverrqml src/serverrqml.cpp

#)
#target_link_libraries(serverrqml ${catkin_LIBRARIES})
#target_link_libraries(serverrqml
 #       Qt5::Core
  #      Qt5::Widgets
   #     Qt5::Quick
    #    Qt5::Sql
     #   Qt5::Multimedia
      #  Qt5::Charts

      #  )
#add_dependencies(serverrqml server_ros_generate_messages_cpp)






## Mark cpp header files for installation
install(DIRECTORY include/server_ros/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.h"
)

#add_library(heat_detection src/heat_detection.cpp)
#add_dependencies(heat_detection ${${PROJECT_NAME}_EXPORTED_TARGETS})
#target_link_libraries(heat_detection ${catkin_LIBRARIES})

#add_executable(heat_detection_node src/heat_detection_node.cpp)
#target_link_libraries(heat_detection_node heat_detection)

#add_library(heat_detection_nodelet src/heat_detection_nodelet.cpp)
#target_link_libraries(heat_detection_nodelet heat_detection)
